from app import app
import pytest

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_login(client):
    # Test successful login
    response = client.post('/login', data={'username': 'test_user', 'password': 'test_password'})
    assert response.status_code == 302  # Redirected to upload page after successful login
    assert b'Login successful!' in response.data  # Ensure success message is present